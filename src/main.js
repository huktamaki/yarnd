var words = null
var wordsCopy = null
var usedWords = []

function preload() {
  words = loadStrings('txt/en_EN.txt')
}

function setup() {
  for (var i = 0; i < words.length; i++) {
    words[i] = words[i].toUpperCase()
  }
  wordsCopy = JSON.parse(JSON.stringify(words))
  document.getElementById("last-word").textContent = words[Math.floor(Math.random() * words.length)]
  usedWords.push(document.getElementById("last-word").textContent)
  wordsCopy.splice(wordsCopy.indexOf(usedWords[usedWords.length - 1]), 1)
}

window.onload = function() {
  document.getElementById("input").focus()
  document.getElementById("input").value = ""
}

function forceKeyPressUppercase(e) {
  var charInput = e.keyCode
  if ((charInput >= 97) && (charInput <= 122)) { // lowercase
    if (!e.ctrlKey && !e.metaKey && !e.altKey) { // no modifier key
      var newChar = charInput - 32
      var start = e.target.selectionStart
      var end = e.target.selectionEnd
      e.target.value = e.target.value.substring(0, start) + String.fromCharCode(newChar) + e.target.value.substring(end)
      e.target.setSelectionRange(start + 1, start + 1)
      e.preventDefault()
    }
  }
}
document.getElementById("input").addEventListener("keypress", forceKeyPressUppercase, false)

var state = 0

document.getElementById("submit").addEventListener("click", function() {
  if (document.getElementById("input").value.length !== 0) {
    if (document.getElementById("last-word").textContent.slice(-1) === document.getElementById("input").value.charAt(0)) {
      if (words.includes(document.getElementById("input").value) === true) {
        if (usedWords.indexOf(document.getElementById("input").value) === -1) {
          var endFill = document.getElementById("end-fill")

          document.getElementById("centered").removeChild(document.getElementById("centered").lastChild)
          document.getElementById("last-word").id = "word" + state

          var wordToAdd = document.getElementById("input").value.toUpperCase()
          usedWords.push(wordToAdd)
          wordsCopy.splice(wordsCopy.indexOf(usedWords[usedWords.length - 1]), 1)

          if (state % 2 === 0) {
            if (state > 0) {
              $("#centered").append("<span class='player' id='last-word' style='margin-left: -32.7px'>" + wordToAdd + "</span>")
            } else {
              $("#centered").append("<span class='player' id='last-word'>" + wordToAdd + "</span>")
            }
            state++
          } else {
            if (state > 0) {
              $("#centered").append("<span class='computer' id='last-word' style='margin-left: -32.7px'>" + wordToAdd + "</span>")
            } else {
              $("#centered").append("<span class='computer' id='last-word'>" + wordToAdd + "</span>")
            }
            state++
          }

          endFill.setAttribute("id", "end-fill")
          endFill.setAttribute("class", "end-fill")
          endFill.setContent = "X"
          document.getElementById("centered").appendChild(endFill)

          document.getElementById("input").value = ""

          window.location.href = "#end-fill"
          document.getElementById("input").focus()
        } else {
          var modal = document.getElementById("errorUsedWord")
          modal.style.display = "block"
          setTimeout(function() {
            modal.style.display = "none"
            document.getElementById("input").value = ""
            document.getElementById("input").focus()
          }, 10000)
          window.onclick = function(event) {
            if (event.target == modal) {
              modal.style.display = "none"
              document.getElementById("input").value = ""
              document.getElementById("input").focus()
            }
          }
        }
      } else {
        var modal = document.getElementById("errorDictionary")
        modal.style.display = "block"
        setTimeout(function() {
          modal.style.display = "none"
          document.getElementById("input").value = ""
          document.getElementById("input").focus()
        }, 10000)
        window.onclick = function(event) {
          if (event.target == modal) {
            modal.style.display = "none"
            document.getElementById("input").value = ""
            document.getElementById("input").focus()
          }
        }
      }
    } else {
      var modal = document.getElementById("errorFirstMatch")
      modal.style.display = "block"
      setTimeout(function() {
        modal.style.display = "none"
        document.getElementById("input").value = ""
        document.getElementById("input").focus()
      }, 10000)
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none"
          document.getElementById("input").value = ""
          document.getElementById("input").focus()
        }
      }
    }
  } else {
    var modal = document.getElementById("errorNoWord")
    modal.style.display = "block"
    setTimeout(function() {
      modal.style.display = "none"
      document.getElementById("input").focus()
    }, 10000)
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none"
        document.getElementById("input").focus()
      }
    }
  }
})

document.getElementById("input").addEventListener("keypress", function(event) {
  if (event.keyCode === 13) {
    if (document.getElementById("input").value.length !== 0) {
      if (document.getElementById("last-word").textContent.slice(-1) === document.getElementById("input").value.charAt(0)) {
        if (words.includes(document.getElementById("input").value) === true) {
          if (usedWords.indexOf(document.getElementById("input").value) === -1) {
            var endFill = document.getElementById("end-fill")

            document.getElementById("centered").removeChild(document.getElementById("centered").lastChild)
            document.getElementById("last-word").id = "word" + state

            var wordToAdd = document.getElementById("input").value.toUpperCase()
            usedWords.push(wordToAdd)
            wordsCopy.splice(wordsCopy.indexOf(usedWords[usedWords.length - 1]), 1)

            if (state % 2 === 0) {
              if (state > 0) {
                $("#centered").append("<span class='player' id='last-word' style='margin-left: -32.7px'>" + wordToAdd + "</span>")
              } else {
                $("#centered").append("<span class='player' id='last-word'>" + wordToAdd + "</span>")
              }
              state++
            } else {
              if (state > 0) {
                $("#centered").append("<span class='computer' id='last-word' style='margin-left: -32.7px'>" + wordToAdd + "</span>")
              } else {
                $("#centered").append("<span class='computer' id='last-word'>" + wordToAdd + "</span>")
              }
              state++
            }

            endFill.setAttribute("id", "end-fill")
            endFill.setAttribute("class", "end-fill")
            endFill.setContent = "X"
            document.getElementById("centered").appendChild(endFill)

            document.getElementById("input").value = ""

            window.location.href = "#end-fill"
            document.getElementById("input").focus()
          } else {
            var modal = document.getElementById("errorUsedWord")
            modal.style.display = "block"
            setTimeout(function() {
              modal.style.display = "none"
              document.getElementById("input").value = ""
              document.getElementById("input").focus()
            }, 10000)
            window.onclick = function(event) {
              if (event.target == modal) {
                modal.style.display = "none"
                document.getElementById("input").value = ""
                document.getElementById("input").focus()
              }
            }
          }
        } else {
          var modal = document.getElementById("errorDictionary")
          modal.style.display = "block"
          setTimeout(function() {
            modal.style.display = "none"
            document.getElementById("input").value = ""
            document.getElementById("input").focus()
          }, 10000)
          window.onclick = function(event) {
            if (event.target == modal) {
              modal.style.display = "none"
              document.getElementById("input").value = ""
              document.getElementById("input").focus()
            }
          }
        }
      } else {
        var modal = document.getElementById("errorFirstMatch")
        modal.style.display = "block"
        setTimeout(function() {
          modal.style.display = "none"
          document.getElementById("input").value = ""
          document.getElementById("input").focus()
        }, 10000)
        window.onclick = function(event) {
          if (event.target == modal) {
            modal.style.display = "none"
            document.getElementById("input").value = ""
            document.getElementById("input").focus()
          }
        }
      }
    } else {
      var modal = document.getElementById("errorNoWord")
      modal.style.display = "block"
      setTimeout(function() {
        modal.style.display = "none"
        document.getElementById("input").focus()
      }, 10000)
      window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none"
          document.getElementById("input").focus()
        }
      }
    }
  }
})

// TODO: add continuation possibility checker function

// TODO: add title screen - YARND / A WORD CHAIN GAME
// TODO: add language selection screen?
// TODO: add restart option? ↺ add auto option? ⇉
